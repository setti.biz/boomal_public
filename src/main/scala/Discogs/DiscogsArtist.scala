package Discogs

case class DiscogsArtist(
                     id: Int,
                     thumb: String,
                     title: String,
                 )