package Discogs

case class Release(
                      status: Option[String],
                      thumb: String,
                      artist: String,
                      label: Option[String],
                      main_release: Option[Int],
                      title: String,
                      role: String,
                      year: Option[Int],
                      `type`: Option[String],
                      resource_url: String,
                      id: Int)