package Discogs

import Boomal.Config
import Discogs.DiscogsClient.{DiscogsResponse, ReleasesPageResponse}
import org.json4s.jackson.JsonMethods.parse
import org.json4s.{DefaultFormats, JValue}

class ArtistReleasesRepository(val urlBuilder: UrlBuilder, client: DiscogsApiClient) {

    implicit val formats: DefaultFormats.type = DefaultFormats

    def getAll(artistId: Int): DiscogsResponse = {
        client.request(urlBuilder.getArtistReleasesUrl(artistId))
    }

    def getArtistReleases(
                             artistId: Int,
                             page: Int = 1,
                             sortBy: String = "year",
                             sortDirection: String = "desc",
                         ): ReleasesPageResponse = {

        val releasesUrl = urlBuilder.getArtistReleasesUrl(artistId, page, sortBy, sortDirection)

        val json = client.request(releasesUrl).json()
        val responseJson = parse(json)

        val releases = (responseJson \ "releases").extract[List[Release]]
        val curPage = (responseJson \ "pagination" \ "page").extract[Int]
        val pages = (responseJson \ "pagination" \ "pages").extract[Int]

        ReleasesPageResponse(releases, curPage, pages)
    }

    def search(query: String): JValue = {
        val result = client.request(urlBuilder.get(Config.getString("discogs.search_url"), List("q" -> query)))

        val resultJson = parse(result.json())

        resultJson \ "results"
    }

}
