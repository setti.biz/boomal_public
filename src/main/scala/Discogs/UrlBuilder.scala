package Discogs


import Boomal.Config
import com.netaporter.uri.dsl._

class UrlBuilder() {

    def get(path: String, params: Seq[(String, Any)]): String = {

        val mergedParams = List(
            "key" -> Config.getString("discogs.key"),
            "secret" -> Config.getString("discogs.secret"),
        ) ++ params

        val uri = Config.getString("discogs.url_prefix") + path
        uri addParams mergedParams
    }

    def getArtistReleasesUrl(
                                artistId: Int,
                                page: Int = 1,
                                sortBy: String = "year",
                                sortDirection: String = "desc",
                            ): String = {

        val prefix = Config.getString("discogs.artist_releases_url")
            .replace("{artist_id}", artistId.toString)

        get(prefix, List("sort_order" -> sortDirection, "sort" -> sortBy, "page" -> page))

    }
}
