package Discogs

trait DiscogsModule {
    import com.softwaremill.macwire._

    lazy val urlBuilder: UrlBuilder = wire[UrlBuilder]
    lazy val client: DiscogsApiClient = wire[DiscogsApiClient]
    lazy val discogsReleaseRepository: ArtistReleasesRepository = wire[ArtistReleasesRepository]
}

object DiscogsModule extends DiscogsModule {

}

