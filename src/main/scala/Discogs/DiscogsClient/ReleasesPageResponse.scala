package Discogs.DiscogsClient

case class ReleasesPageResponse(
                                   releases: List[Discogs.Release],
                                   page: Int,
                                   pages: Int,
                               )