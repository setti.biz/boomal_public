package Discogs.DiscogsClient

import Boomal.Response

class DiscogsResponse(val j: String) extends Response {
    def json(): String = j
}
