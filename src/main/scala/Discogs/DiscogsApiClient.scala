package Discogs


import Discogs.DiscogsClient.DiscogsResponse

import scalaj.http.{Http, HttpResponse}

class DiscogsApiClient {

    def request(url: String): DiscogsResponse = {

        val response: HttpResponse[String] =
            Http(url)
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("User-Agent", "BoomAl Discogs client")
                .asString

        new DiscogsResponse(response.body.toString)
    }

}

