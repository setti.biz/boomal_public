package Boomal

trait AbstractUrlBuilder {

    val baseUrl: String

    def getArtistReleasesUrl(
                                artistId: Int,
                                sortBy: String = "year",
                                sortDirection: String = "desc",
                            ): String

    def get(path: String, params: Seq[(String, Any)]): String
}
