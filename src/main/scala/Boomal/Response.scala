package Boomal

trait Response {
    def json(): String
}