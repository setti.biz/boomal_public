package Boomal

import com.typesafe.config.ConfigFactory

class Config {

    def getString(key: String): String = {
        val conf = ConfigFactory.load()
        conf.getString(key)
    }

}

object Config extends Config