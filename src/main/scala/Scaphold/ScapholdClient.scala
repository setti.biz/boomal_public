package Scaphold

import org.json4s.JsonAST.JValue

import sangria.ast.Document
import sangria.parser.QueryParser
import sangria.renderer.QueryRenderer

import scala.util.Success
import scalaj.http._

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._


class ScapholdClient(dbHost: String)  {

    implicit val formats = DefaultFormats

    def createArtist(name: String): List[Scaphold.ScapholdArtist] = {
        val createArtistQl =
            """mutation CreateArtist($artist: CreateArtistInput!) {
                  createArtist(input: $artist) {
                    changedArtist{
                        id
                        name
                      }
               	 }
              }

           """

        val response: JValue = performQuery(createArtistQl)

        val adgesJson = response \ "data" \ "createArtist" \ "changedArtist"

        adgesJson.extract[List[Node[ScapholdArtist]]] map (_.node)
    }


    def getLastUpdatedArtist(name: Option[String]): List[Scaphold.ScapholdArtist] = {

        implicit val formats: DefaultFormats.type = DefaultFormats

        val artistsQueryQl =
            """query
                 GetAllArtists {
                   viewer {
                     allArtists {
                       edges {
                         node {
                           id
                           externalId
                           name
                         }
                       }
                     }
                   }
                 }
               """

        val response: JValue = performQuery(artistsQueryQl)

        val adgesJson = response \ "data" \ "viewer" \ "allArtists" \ "edges"

        adgesJson.extract[List[Node[ScapholdArtist]]] map (_.node)
    }


    private def performQuery(artistsQueryQl: String, variables: Option[Map[String, String]] = None) = {

        implicit val formats = DefaultFormats

        val Success(document: Document) = QueryParser.parse(artistsQueryQl)
        val compactQl = QueryRenderer.render(document, QueryRenderer.Compact).stripMargin

        val json = ("query" -> compactQl) ~ ("variables" -> "")
        val artistsQueryJson = compact(render(json))

        val response: HttpResponse[String] =
            Http(dbHost)
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .postData(artistsQueryJson)
                .asString

        val responseString = response.body

        parse(responseString.toString)
    }
}
