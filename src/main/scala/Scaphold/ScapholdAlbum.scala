package Scaphold

case class ScapholdAlbum(
                            artist: ScapholdArtist,
                            modifiedAt: String,
                            createdAt: String)
