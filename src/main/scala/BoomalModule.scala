import Scaphold.ScapholdClient
import com.google.inject.name.Names
import com.google.inject.{AbstractModule, Guice, Injector}
import net.codingwell.scalaguice.ScalaModule


class BoomalModule extends AbstractModule with ScalaModule {
    def configure(): Unit = {
        val sc = new ScapholdClient("https://us-west-2.api.scaphold.io/graphql/boomal")
        bind[ScapholdClient].toInstance(sc)
        // bind[ExampleDiClassChild].toInstance(new ExampleDiClassChild)
    }
}

object BoomalModule{

    import net.codingwell.scalaguice.InjectorExtensions._

    val injector: Injector = Guice.createInjector(new BoomalModule())
    val scapholdClient: ScapholdClient = injector.instance[ScapholdClient]

}
