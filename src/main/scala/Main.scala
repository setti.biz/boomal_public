import Release.{CheckReleasesActor, Page}
import akka.actor.{ActorSystem, Props}

import scala.language.postfixOps

object Main {
    def main(args: Array[String]) {
        println("Hi there!")
        val system = ActorSystem("releasesChecker")

        val helloActor = system.actorOf(Props[CheckReleasesActor], name = "releasesActor")

        helloActor ! Page(1, 5)


    }
}
