package Artist

trait ArtistsRepository {
    def lastUpdatedArtist(): Option[BoomalArtist]
    def findArtistByName(name: String): Option[BoomalArtist]
    def findArtistById(id: String): Option[BoomalArtist]
}
