package Artist

import Release.BoomalRelease

trait Artist{
    var thumb: String
    var title: String
    var releases: List[BoomalRelease]
}
