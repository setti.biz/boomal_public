package Artist

import Release.BoomalRelease
import org.mongodb.scala.bson.ObjectId

case class BoomalArtist(_id: ObjectId, var thumb: String, var title: String,
                        var releases: List[BoomalRelease] = List[BoomalRelease]()) extends Artist {
}

object BoomalArtist {
    def apply(thumb: String, title: String, releases: List[BoomalRelease]): BoomalArtist =
        new BoomalArtist(new ObjectId(), thumb, title, releases)

}
