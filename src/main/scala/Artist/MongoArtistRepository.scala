package Artist

import BoomalPersistance.MongoConnector

class MongoArtistRepository extends  ArtistsRepository {
    override def lastUpdatedArtist(): Option[BoomalArtist] = MongoConnector.lastUpdatedArtist()

    override def findArtistByName(name: String): Option[BoomalArtist] = MongoConnector.findArtistByName(name)

    override def findArtistById(id: String): Option[BoomalArtist] = MongoConnector.findArtistById(id)
}
