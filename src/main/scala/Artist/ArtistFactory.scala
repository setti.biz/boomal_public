package Artist

import com.softwaremill.quicklens._

class ArtistFactory(repository: ArtistsRepository) {
    def getArtist(id: String): Option[Artist] = {
        val artist = repository.findArtistById(id).get

        Some(artist.modify(_.releases).using(_.sortWith(_.date.getMillis > _.date.getMillis)))
    }
}

object ArtistFactory {

}
