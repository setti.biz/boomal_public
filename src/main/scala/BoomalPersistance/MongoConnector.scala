package BoomalPersistance

import Artist.BoomalArtist
import Release.BoomalRelease
import com.mongodb.connection.ClusterSettings
import org.bson.codecs.configuration.CodecRegistry

import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success, Try}

// Mongo
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._

import scala.concurrent.duration._
import scala.collection.JavaConverters._

object MongoConnector {


    val codeRegistry1: CodecRegistry = fromRegistries(
        fromProviders(
            classOf[BoomalArtist],
            classOf[BoomalRelease]
        ),
        CodecRegistries.fromCodecs(new JodaCodec),
        DEFAULT_CODEC_REGISTRY
    )

    // or provide custom MongoClientSettings
    val clusterSettings: ClusterSettings = ClusterSettings.builder()
        .hosts(List(new ServerAddress("localhost")).asJava).build()

    val settings: MongoClientSettings = MongoClientSettings.builder()
        .clusterSettings(clusterSettings).build()

    val mongoClient: MongoClient = MongoClient(settings)

    val database: MongoDatabase = mongoClient.getDatabase("mydb")
        .withCodecRegistry(codeRegistry1)


    val collection: MongoCollection[BoomalArtist] = database.getCollection("test")

    def findArtistById(id: String): Option[BoomalArtist] = {
        val artist = collection.find(equal("id", id)).first().toFuture()
        getArtist(artist)
    }

    def lastUpdatedArtist(): Option[BoomalArtist] = {
        val artist = collection.find(equal("date", "")).first().toFuture()
        getArtist(artist)
    }

    def findArtistByName(name: String): Option[BoomalArtist] = {
        val artist = collection.find(equal("title", name)).first().toFuture()
        getArtist(artist)
    }


    private def getArtist(artist: Future[BoomalArtist]) = {
        Try(Await.result(artist, 10 seconds)) match {
            case Success(extractedVal) => Some(extractedVal)
            case Failure(_) => None
            case _ => None
        }
    }

}
