package BoomalPersistance

class PersistenceModule {
    import com.softwaremill.macwire._

    lazy val theDatabaseAccess   = wire[BoomalPersistance.Database]
}
