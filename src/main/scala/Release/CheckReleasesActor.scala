package Release

import Discogs.DiscogsClient.ReleasesPageResponse
import Discogs.{ArtistReleasesRepository, DiscogsModule}
import akka.actor.{Actor, ActorRef, PoisonPill, Timers}

import scala.concurrent.duration._
import scala.language.postfixOps


class CheckReleasesActor(result: ActorRef , interval: FiniteDuration = 2 seconds, artistId: Int) extends Actor with Timers {

    val releasesRepository: ArtistReleasesRepository
        = DiscogsModule.discogsReleaseRepository

    def receive: PartialFunction[Any, Unit] = {
        case CheckReleasesActor.Start =>
            println(s"Task started. Current page: 1")
            val releases: ReleasesPageResponse = releasesRepository.getArtistReleases(this.artistId)
            deferredMsg(releases)
        case p: ReleasesPageResponse if p.page > p.pages =>
            sender() ! PoisonPill
            println(s"Bye! Current page: " + p.page.toString)
            result ! p
            context.system.terminate()
        case p: ReleasesPageResponse =>
            deferredMsg(p)
            println(s"Tick " + p.page.toString)
    }

    private def deferredMsg(releases: ReleasesPageResponse): Unit = {
        timers.startSingleTimer(
            TickKey,
            (releases.releases, releases.page+1, releases.pages),
            interval
        )
    }

    private case object TickKey
}

object CheckReleasesActor {
    case object Start
}
