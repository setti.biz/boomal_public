package Release

import Artist.BoomalArtist
import org.joda.time.DateTime

class BoomalRelease(
                    val title: String,
                    val date: DateTime) extends Release {

    var id: Int = 0

    var thumb: String = ""

    var _artist: BoomalArtist = _

    def assignToArtist(a: BoomalArtist): Unit = {
        this._artist = a
    }

    def artist(): BoomalArtist = this._artist

}
