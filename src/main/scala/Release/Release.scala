package Release

import Artist.BoomalArtist
import org.joda.time.DateTime

trait Release {
    val title: String
    val date: DateTime
}
