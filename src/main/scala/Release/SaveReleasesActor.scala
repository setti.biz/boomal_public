package Release

import akka.actor.{Actor, Timers}

import scala.concurrent.duration._
import scala.language.postfixOps


class SaveReleasesActor extends Actor with Timers {

    private case object TickKey
    private case object FirstTick

    timers.startSingleTimer(TickKey, FirstTick, 500 millis)

    var page: Int = 1

    def receive: PartialFunction[Any, Unit] = {
        case p: Page =>
            println(s"Tick " + page.toString)
            page = page + 1
    }
}
