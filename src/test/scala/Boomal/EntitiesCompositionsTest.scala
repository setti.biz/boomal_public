package Boomal

import Artist.BoomalArtist
import Release.BoomalRelease
import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Matchers}

class EntitiesCompositionsTest extends FlatSpec with Matchers {

    "An BoomalArtist" should "be abel be composed with its releases list" in {

        val release1 = new BoomalRelease(title = "Album title 1", date = DateTime.parse("2001-10-15"))

        val release2 = new BoomalRelease(title = "Album title 2", date = DateTime.parse("2001-10-15"))

        val artist = BoomalArtist (
            thumb = "",
            title = "Nirvana",
            releases = List[BoomalRelease](release1, release2)
        )

        val titles = artist.releases map {
            _.title
        }

        titles shouldEqual List("Album title 1", "Album title 2")

        1+1

    }


}