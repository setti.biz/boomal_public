import org.scalatest._
import sandbox.DiModule

import scala.language.postfixOps

class DiTest extends FlatSpec with Matchers {


  "DI module" should "instantiate withe child via Google Guice" in {
    val diModule = DiModule
    val greater = diModule.cl.child.hello
      greater shouldEqual "hello"
  }

}
