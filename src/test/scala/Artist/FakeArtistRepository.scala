package Artist

import Release.BoomalRelease
import org.joda.time.DateTime

class FakeArtistRepository extends ArtistsRepository {

    val r1 = new BoomalRelease(title = "Album title 1", date = DateTime.parse("2001-10-15"))
    val r2 = new BoomalRelease(title = "Album title 2", date = DateTime.parse("2014-10-05"))
    val r3 = new BoomalRelease(title = "Album title 3", date = DateTime.parse("2014-10-28"))
    val r4 = new BoomalRelease(title = "Album title 4", date = DateTime.parse("2015-11-15"))
    val r5 = new BoomalRelease(title = "Album title 4", date = DateTime.parse("2017-09-15"))

    val someArtist = Some(Artist.BoomalArtist.apply(
        thumb = "",
        title = "Nirvana",
        releases = List(r1, r2, r3, r4, r5))
    )

    override def lastUpdatedArtist(): Option[BoomalArtist] = {
        someArtist
    }

    override def findArtistByName(name: String): Option[BoomalArtist] = {
        someArtist
    }

    override def findArtistById(id: String): Option[BoomalArtist] = {
        someArtist
    }
}
