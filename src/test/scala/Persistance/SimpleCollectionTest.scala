package Persistance

import Artist.BoomalArtist
import BoomalPersistance.JodaCodec
import Release.BoomalRelease
import com.mongodb.connection.ClusterSettings
import org.bson.codecs.configuration.CodecRegistries
import org.joda.time.DateTime
import org.scalatest.{FlatSpec, Ignore, Matchers}

// Mongo
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala._
import org.mongodb.scala.model.Filters._

import BoomalPersistance.Helpers._

import scala.collection.JavaConverters._

@Ignore
class SimpleCollectionTest extends FlatSpec with Matchers {

    "A simple collection" should "return the database's typed items" in {

        val release1 = new BoomalRelease("Album title 1", date = DateTime.parse("2001-10-15"))
        val release2 = new BoomalRelease("Album title Abc", date = DateTime.parse("2001-10-15"))
        val release3 = new BoomalRelease("Album title 2", date = DateTime.parse("2001-10-15"))

        val artist = BoomalArtist(thumb = "", title = "Nirvana", releases = List(release1, release2, release3))

        val codeRegistry1 = fromRegistries(
            fromProviders(
                classOf[BoomalArtist],
                classOf[BoomalRelease]
            ),
            CodecRegistries.fromCodecs(new JodaCodec),
            DEFAULT_CODEC_REGISTRY
        )

        // or provide custom MongoClientSettings
        val clusterSettings: ClusterSettings = ClusterSettings.builder()
            .hosts(List(new ServerAddress("localhost")).asJava).build()

        val settings: MongoClientSettings = MongoClientSettings.builder()
            .clusterSettings(clusterSettings).build()

        val mongoClient: MongoClient = MongoClient(settings)

        val database = mongoClient.getDatabase("mydb")
            .withCodecRegistry(codeRegistry1)

        val collection: MongoCollection[BoomalArtist] = database.getCollection("test")

        collection.insertOne(artist).results()

        val connector = BoomalPersistance.MongoConnector
        connector.findArtistByName("Nirvana").get.title shouldEqual artist.title
    }


    "Artist" should "be found by our persistent mongo module" in {

        val release1 = new BoomalRelease("Album title 1", date = DateTime.parse("2001-10-15"))
        val release2 = new BoomalRelease("Album title Abc", date = DateTime.parse("2001-10-15"))
        val release3 = new BoomalRelease("Album title 2", date = DateTime.parse("2001-10-15"))

        val artist = BoomalArtist(thumb = "", title = "Nirvana", releases = List(release1, release2, release3))

        val connector = BoomalPersistance.MongoConnector

        connector.findArtistByName("Nirvana").get.title shouldEqual artist.title
    }

}