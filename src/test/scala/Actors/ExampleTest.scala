package Actors

import Release.{CheckReleasesActor, Page}
import akka.actor.{Actor, ActorRef, ActorSystem, PoisonPill, Props, Timers}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest._

import scala.concurrent.duration._

case class Increment(num: Int)

class RecursionActorWithTimers() extends Actor with Timers {

    def receive: PartialFunction[Any, Unit] = {
        case a: ActorRef =>
            context.self ! Increment(1)
        case in: Increment if in.num == 10 =>
            println(in.num)
            sender() ! PoisonPill
        case in: Increment =>
            println(in.num)
            sender() ! Increment(in.num + 1)
    }

    private case object TickKey

    timers.startSingleTimer(TickKey, FirstTick, 500 millis)

    private case object FirstTick

}


class ExampleTest() extends TestKit(ActorSystem("ExampleTest")) with ImplicitSender
    with WordSpecLike with Matchers with BeforeAndAfterAll {

    override def afterAll {
        TestKit.shutdownActorSystem(system)
    }


    "An incremental actors actor" must {

        "increment messages to each other" in {

            val ra = system.actorOf(Props(classOf[RecursionActorWithTimers]))

            ra ! ra

            // probe1.receiveN(10)
        }

    }

}
