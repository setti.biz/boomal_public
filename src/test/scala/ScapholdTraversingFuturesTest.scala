
import org.scalatest.{FeatureSpec, GivenWhenThen, Ignore}

@Ignore
class ScapholdTraversingFuturesTest extends FeatureSpec with GivenWhenThen {


        scenario("one") {
            Given("Scaphold client")
            val scapholdClient = BoomalModule.scapholdClient
            When("I perform the request of last 10 artists")
            val lastArtist = scapholdClient.getLastUpdatedArtist(None)
            assert(lastArtist.nonEmpty)
        }

        scenario("two") {

            import sangria.ast.Document
            import sangria.parser.QueryParser
            import sangria.renderer.QueryRenderer

            import scala.util.Success

            val query =
                """{
                     query
                     GetAllArtists {
                       viewer {
                         allArtists {
                           edges {
                             node {
                               id
                               name
                             }
                           }
                         }
                       }
                     }
                   }"""

            // Parse GraphQl query
            val Success(document: Document) = QueryParser.parse(query)

            // Pretty rendering of GraphQl query as a `String`
            println(QueryRenderer.render(document))

            // Compact rendering of GraphQl query as a `String`
            println(QueryRenderer.render(document, QueryRenderer.Compact))

        }


}
