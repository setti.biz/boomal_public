package Discogs

import org.scalatest.{FlatSpec, Matchers}

class DiscogsVariousRequestsTest extends FlatSpec with Matchers  {

    "Discogs" should "return artists's releases with meta" in {
        val repo = DiscogsModule.discogsReleaseRepository
        val result = repo.getArtistReleases(125246, 3)

        result.page should equal(3)
    }

}
