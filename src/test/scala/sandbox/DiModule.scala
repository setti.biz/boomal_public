package sandbox

import java.lang.reflect.Constructor

import com.google.inject.{AbstractModule, Guice, Injector, PrivateModule}
import net.codingwell.scalaguice.{ScalaModule, ScalaPrivateModule}

class DiModule extends AbstractModule with ScalaModule {
    def configure(): Unit = {
        // bind[ExampleDiClass]
        // bind[ExampleDiClassChild].toInstance(new ExampleDiClassChild)
    }
}

object DiModule{

    import net.codingwell.scalaguice.InjectorExtensions._

    val injector: Injector = Guice.createInjector(new DiModule())
    val cl: ExampleDiClass = injector.instance[ExampleDiClass]

}