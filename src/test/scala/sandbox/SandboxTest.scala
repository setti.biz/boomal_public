package sandbox

import Artist.FakeArtistRepository
import org.scalatest._

import scala.language.postfixOps


class SandboxTest extends FlatSpec with Matchers {

    "A sandbox for implicit" should "call trainings" in {

        case class Config(user: String)

        def someConstructor(implicit config: Config): String = config.user

        implicit val myConfig: Config = Config("another_user")

        val result = someConstructor

        1 + 1

    }

    "Compose" should "behave like a chain" in {

        val cap = (s: String) => s.capitalize
        val rev = (s: String) => s.reverse
        val tr = (s: String) => s.trim
        val rp = (s: String) => s.replaceAll(" ", "")

        val fancyString = cap compose rev compose tr compose rp

        fancyString(" my string ") shouldEqual "Gnirtsym"
    }


    "Maps playing" should "" in {

        var myMap = Map[String, String]()

        val newMap = myMap + ("q" -> "Nirvana")

        val lList = List(22, 33, 44)

        var nirvana = newMap("q")

        val lMap = (lList.indices zip lList).toMap

        val m3 = lMap(1)

        def strToInt(s: String): Int = s.toInt

        def intMultiplicator(i: Int) = i * i

        val stringMultoplicator = intMultiplicator _ compose strToInt

        val result = stringMultoplicator("3")

        def multiplier(x: Int, y: Int): Int = x * y

        def multuplier2 = (y: Int) => multiplier(2, y)

        val v12 = multuplier2(6)

        1 + 1
    }

    "Parentheses " should "transparently used with curly braces" in {

        def toInt(s: String): Int = s.toInt

        val myInt = toInt {
            val numberedString1 = "1"
            val numberedString2 = "2"
            numberedString1 + numberedString2
        } * 10

        1 + myInt shouldBe 121
    }

    "Map" should "act with Options" in {
        val intsO = List(Option(1), Option(2), None, Option(10))
        val doubled = intsO map (_ getOrElse 0 * 2) filter (_ > 1) map (Some(_))

        doubled
    }

    "Implicit class" should "extend Int type" in {

        // See http://allaboutscala.com/tutorials/chapter-3-beginner-tutorial-using-classes-scala/scala-tutorial-learn-use-implicit-class-extension-methods/

        case class Donut(name: String, price: Double, productCode: Option[Long] = None)

        val vanillaDonut: Donut = Donut("Vanilla", 1.5, Some(1))

        object DonutImplicits {

            implicit class AugmentedDonut(donut: Donut) {
                def uuid: String = s"${donut.name} - ${donut.price} - ${donut.productCode.getOrElse(12345)}"
            }

        }

        import DonutImplicits._

        vanillaDonut.uuid shouldBe "Vanilla - 1.5 - 1"


    }


    "Curry" should "be correctly implemented" in {

        def curry[A, B, C](f: (A, B) => C): A => (B => C) = a => b => f(a, b)

    }

    "Sorting training" should "sort an list of objects" in {
        val artistRepo = new FakeArtistRepository
        val artist = artistRepo.findArtistById("any").get

        artist.releases.sortWith(_.date.getMillis > _.date.getMillis).head.date.toString("yyy-MM-dd") shouldEqual "2017-09-15"

        artist.releases.sortWith(_.date.getMillis < _.date.getMillis).head.date.toString("yyy-MM-dd") shouldEqual "2001-10-15"
    }

    "Object" should "be able to extended" in {
        1+1 should be (2)
    }
}
