import org.json4s.jackson.JsonMethods.{compact, render}
import org.scalatest._

import scala.language.postfixOps

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._

class JsonTest extends FlatSpec with Matchers {

    "JSON building dependant on optional value" should "be made in different ways" in {

        def buildJson(optionalArg: Option[Map[String, String]] = None): String = {

            val json = ("query" -> "some string value") ~ ("variables" -> optionalArg)

            compact(render(json))
        }

        buildJson(Some(Map("key" -> "value"))) shouldEqual
            """{"query":"some string value","variables":{"key":"value"}}"""

        buildJson() shouldEqual
            """{"query":"some string value"}"""
    }

}
