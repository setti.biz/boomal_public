name := "boomal"
packageName in Linux := "boomal"

organization := "biz.setti"

version := "1.2.0"

scalaVersion := "2.12.4"

maintainer := "Serhii Setti <setti@setti.biz>"

packageSummary := "Hello Deploy Debian Package"

packageDescription := """A fun package for Linux deployment testing"""

// exportJars := true
autoScalaLibrary := true

//scalaHome := Some(file("/usr/share/scala/"))

// enablePlugins(JavaAppPackaging)

// set the main class for packaging the main jar
//mainClass in (Compile, run) := Some("Main")
//mainClass in (Compile, packageBin) := Some("Main")

libraryDependencies += "org.scala-lang" % "scala-library" % scalaVersion.value % "test"
libraryDependencies += "com.typesafe" % "config" % "1.3.1"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"

// Web
libraryDependencies += "org.scalaj" %% "scalaj-http" % "2.3.0"
libraryDependencies += "io.spray" %%  "spray-json" % "1.3.3"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.0"
libraryDependencies += "org.json4s" % "json4s-jackson_2.12" % "3.5.3"
libraryDependencies += "io.lemonlabs" %% "scala-uri" % "0.5.0"

// DI
libraryDependencies += "net.codingwell" %% "scala-guice" % "4.1.1"
libraryDependencies += "com.softwaremill.macwire" %% "macros" % "2.3.0" % "provided"
libraryDependencies += "com.softwaremill.macwire" %% "macrosakka" % "2.3.0" % "provided"
libraryDependencies += "com.softwaremill.macwire" %% "util" % "2.3.0"
libraryDependencies += "com.softwaremill.macwire" %% "proxy" % "2.3.0"

// Akka
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.6"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.6" % Test

// Tests
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % Test

// GraphQL
libraryDependencies += "org.sangria-graphql" %% "sangria" % "1.3.3"

// DB
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.2.0"

// Util
libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "2.18.0"
libraryDependencies += "com.softwaremill.quicklens" %% "quicklens" % "1.4.11"

